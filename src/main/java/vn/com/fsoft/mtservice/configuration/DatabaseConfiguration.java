package vn.com.fsoft.mtservice.configuration;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import vn.com.fsoft.mtservice.object.entities.ApplicationConfigEntity;
import vn.com.fsoft.mtservice.object.entities.AuthorityEntity;
import vn.com.fsoft.mtservice.object.entities.CredentialTokenEntity;
import vn.com.fsoft.mtservice.object.entities.FunctionPermissionEntity;
import vn.com.fsoft.mtservice.object.entities.MasterDataParameterEntity;
import vn.com.fsoft.mtservice.object.entities.MasterDataTableEntity;
import vn.com.fsoft.mtservice.object.entities.ProfileEntity;
import vn.com.fsoft.mtservice.object.entities.RoleEntity;
import vn.com.fsoft.mtservice.object.entities.UserEntity;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

/**
 * 
 * @author hungxoan
 *
 */

@Configuration("databaseConfiguration")
public class DatabaseConfiguration {

    @Value("${database.driverClassName}")
    private String driverClassName;

    @Value("${database.url}")
    private String jdbcUrl;

    @Value("${database.username}")
    private String userName;

    @Value("${database.password}")
    private String password;

    @Value("${hibernate.dialect}")
    private String dialect;

    @Value("${hibernate.hbm2ddl.auto}")
    private String hbm2dll;

    @Value("${hibernate.show_sql}")
    private String showSql;

    @Value("${hibernate.format_sql}")
    private String formatSql;

    @Value("${hibernate.connection.pool_size}")
    private Integer poolSize;

    @Value(("${jdbc.batch_size}"))
    private Integer batchSize;

    @Bean
    DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(jdbcUrl);
        dataSource.setUsername(userName);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    public HibernateTransactionManager getTransactionManager() {

        HibernateTransactionManager transactionManager =
                new HibernateTransactionManager(getSessionFactory());

        return transactionManager;
    }

    @Bean
    public SessionFactory getSessionFactory() {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(getDataSource());
        sessionFactoryBean.setAnnotatedClasses(

                ApplicationConfigEntity.class,
                CredentialTokenEntity.class,

                MasterDataTableEntity.class,
                MasterDataParameterEntity.class,
                FunctionPermissionEntity.class,

                UserEntity.class,
                ProfileEntity.class,
                RoleEntity.class,
                AuthorityEntity.class
        );

        sessionFactoryBean.setHibernateProperties(getHibernateProperties());

        try {

            sessionFactoryBean.afterPropertiesSet();
        } catch (IOException e) {
            //log exc
        }

        return sessionFactoryBean.getObject();
    }

    private Properties getHibernateProperties() {

        Properties properties = new Properties();
        properties.put("hibernate.dialect", dialect);
        properties.put("hibernate.connection.pool_size", poolSize);
        properties.put("hibernate.hbm2ddl.auto", hbm2dll);
        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.format_sql", formatSql);
        properties.put("jdbc.batch_size", batchSize);

        return properties;
    }
}
