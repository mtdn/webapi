package vn.com.fsoft.mtservice.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import vn.com.fsoft.mtservice.object.converter.HibernateAwareObjectMapper;

/**
 * 
 * @author hungxoan
 *
 */

@Configuration("jacksonConfiguration")
public class JacksonConfiguration {

    // Jackson and a fix for hibernate lazy issue
    @Bean
    public MappingJackson2HttpMessageConverter getJackson2HttpConverter() {

        MappingJackson2HttpMessageConverter httpConverter = new MappingJackson2HttpMessageConverter();
        HibernateAwareObjectMapper hibernateAwareMapper = new HibernateAwareObjectMapper();
        httpConverter.setObjectMapper(hibernateAwareMapper);

        return httpConverter;
    }
}
