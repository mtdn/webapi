package vn.com.fsoft.mtservice.controller;


import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.object.base.ValidationStatus;
import vn.com.fsoft.mtservice.util.JacksonUtils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * 
 * @author hungxoan
 *
 */
public class ExtraFunctionController {

    public final ResponseEntity<String> reject(ValidationStatus validationStatus) {

        String body = rejectDefinition(validationStatus);
        return new ResponseEntity<String>(body, null, HttpStatus.BAD_REQUEST);
    }

    public String rejectDefinition(ValidationStatus validationStatus) {
        return JacksonUtils.java2Json(validationStatus);
    }

    protected HttpStatus getStatus(String code) {
        if(code.equals(HttpConstant.CODE_SUCCESS)) {
            return HttpStatus.OK;
        }

        if(code.equals(HttpConstant.CODE_BAD_REQUEST)) {
            return HttpStatus.BAD_REQUEST;
        }

        if(code.equals(HttpConstant.CODE_NOT_FOUND)) {
            return HttpStatus.NOT_FOUND;
        }

        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
