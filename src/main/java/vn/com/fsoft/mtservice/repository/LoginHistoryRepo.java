package vn.com.fsoft.mtservice.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;


import vn.com.fsoft.mtservice.bean.data.BaseRepo;
import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.ResultTransformer;
import vn.com.fsoft.mtservice.object.entities.LoginHistoryEntity;
import vn.com.fsoft.mtservice.object.form.LoginHistoryForm;
import vn.com.fsoft.mtservice.object.form.command.RestCommandForm;
import vn.com.fsoft.mtservice.object.form.query.LoginHistoryQueryForm;
import vn.com.fsoft.mtservice.object.helper.QueryCallBack;
import vn.com.fsoft.mtservice.object.helper.QueryResult;
import vn.com.fsoft.mtservice.object.helper.TransformQueryResult;
/**
 * 
 * @author hungxoan
 *
 */
@Repository(value = "loginHistoryRepo")
public class LoginHistoryRepo
		extends BaseRepo<LoginHistoryEntity, ResultTransformer, LoginHistoryForm, RestCommandForm> {

	@Value("${record.per.page}")
	private Integer recordPerPage;

	@Value("${datetime.format}")
	private String dateFormat;

	private HibernateTemplate hibernateTemplate;

	public LoginHistoryRepo(@Autowired SessionFactory sessionFactory) {
		super(sessionFactory);
		hibernateTemplate = getHibernateTemplates();
	}

	@Override
	protected RepoStatus<LoginHistoryEntity> findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected RepoStatus<List<LoginHistoryEntity>> findByCode(String code) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	public RepoStatus<Integer> getAllCount() {
		try {
			String hql = "Select count(*) from LoginHistoryEntity";
			Integer count = DataAccessUtils.intResult(hibernateTemplate.find(hql));
			return new RepoStatus<Integer>(HttpConstant.CODE_SUCCESS, "success", count);
		} catch (DataAccessException e) {
			e.printStackTrace();
		}
		return new RepoStatus<>(HttpConstant.CODE_SERVER_ERROR, "Server error");
	}

	@Transactional
	public RepoStatus<Integer> countLogin(Boolean success) {
		try {
			String hql = "Select count(*) from LoginHistoryEntity l";
			Integer count = null;
			if (success != null) {
				hql += " Where l.loginSuccess = ?";
				count = DataAccessUtils.intResult(hibernateTemplate.find(hql, success));
			} else {
				count = DataAccessUtils.intResult(hibernateTemplate.find(hql));
			}

			return new RepoStatus<Integer>(HttpConstant.CODE_SUCCESS, "found", count);
		} catch (DataAccessException e) {
			e.printStackTrace();
		}

		return new RepoStatus<>(HttpConstant.CODE_SERVER_ERROR, "Server error");
	}

	@Transactional
	public RepoStatus addAll(List<LoginHistoryEntity> list) {
		try {
			for (LoginHistoryEntity e : list) {
				hibernateTemplate.save(e);
			}
			return new RepoStatus<>(HttpConstant.CODE_SUCCESS, "acknowledged");
		} catch (DataAccessException e) {
			e.printStackTrace();
		}
		return new RepoStatus<>(HttpConstant.CODE_SERVER_ERROR, "Server error");
	}

	@Override
	public RepoStatus<QueryResult<LoginHistoryEntity>> query(LoginHistoryForm form) {

		Integer page = form.getP();
		if (null == page || 0 == page) {
			page = 1;
		}

		Integer pSize = form.getpSize();
		if (null == pSize || 0 == pSize) {
			pSize = recordPerPage;
		}

		LoginHistoryQueryForm queryForm = form.getQuery();
		String hql = "Select l from LoginHistoryEntity l";
		StringBuilder where = new StringBuilder();
		Map<String, Object> params = new HashMap<>();

		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		try {
			if (queryForm.getFromTime() != null) {
				where.append(" l.loginTime >= :from ");
				Date from = format.parse(queryForm.getFromTime());
				params.put("from", from);
			}

			if (queryForm.getToTime() != null) {
				if (where.length() > 0) {
					where.append(" AND ");
				}
				where.append(" l.loginTime <= :to ");
				Date to = format.parse(queryForm.getToTime());
				params.put("to", to);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (!StringUtils.isEmpty(queryForm.getUserName())) {
			if (where.length() > 0) {
				where.append(" AND ");
			}
			where.append(" l.userName = :username ");
			params.put("username", queryForm.getUserName());
		}

		if (queryForm.getLoginSuccess() != null) {
			if (where.length() > 0) {
				where.append(" AND ");
			}
			where.append(" l.loginSuccess = :success");
			params.put("success", queryForm.getLoginSuccess());
		}

		String[] keySet = new String[params.size()];
		Object[] valSet = new Object[params.size()];
		int count = 0;
		for (String key : params.keySet()) {
			keySet[count] = key;
			valSet[count] = params.get(key);
			count++;
		}

		hql += (where.length() > 0 ? " WHERE " + where.toString() : "");
		QueryCallBack<LoginHistoryEntity> callback = new QueryCallBack<>(hql, keySet, valSet, (page - 1) * pSize,
				pSize);

		List<LoginHistoryEntity> entities = null;
		try {
			entities = hibernateTemplate.execute(callback);
		} catch (DataAccessException e) {
			e.printStackTrace();
			return new RepoStatus("500", "Server got error. Please wait a moment and try it again later.");
		}

		Integer rowCount = callback.getRowCount();
		QueryResult<LoginHistoryEntity> result = new QueryResult<>(rowCount, entities);
		return new RepoStatus<QueryResult<LoginHistoryEntity>>("200", "acknowledged", result);
	}

	@Override
	protected RepoStatus<TransformQueryResult<ResultTransformer>> transformQuery(LoginHistoryForm form) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected RepoStatus<Object> objectQuery(LoginHistoryForm form) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected RepoStatus<Boolean> delete(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected RepoStatus<Boolean> deleteBulk(List<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected RepoStatus<Integer> save(IncomingRequestContext context, RestCommandForm commandForm) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected RepoStatus<Integer> update(IncomingRequestContext context, RestCommandForm commandForm) {
		// TODO Auto-generated method stub
		return null;
	}

}
