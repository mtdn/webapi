package vn.com.fsoft.mtservice.object.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import vn.com.fsoft.mtservice.constants.DatabaseConstants;

/**
 * 
 * @author hungxoan
 *
 */

@Entity(name = "ApplicationConfigEntity")
@Table(name = "application_config", schema = DatabaseConstants.SCHEMA)
public class ApplicationConfigEntity {

    @Id
    private Integer id;

    @Column(name = "key")
    private String key;

    @Column(name = "value")
    private String value;

    public ApplicationConfigEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
