package vn.com.fsoft.mtservice.object.helper;

import com.fasterxml.jackson.annotation.JsonView;

import vn.com.fsoft.mtservice.object.base.ResultTransformer;
import vn.com.fsoft.mtservice.util.Views;

import java.util.List;

/**
 * 
 * @author hungxoan
 *
 * @param <T>
 */
public class TransformQueryResult<T extends ResultTransformer> {

    private Integer rowCount;
    private List<T> objectList;

    public TransformQueryResult(Integer rowCount, List<T> objectList) {
        this.rowCount = rowCount;
        this.objectList = objectList;
    }

    @JsonView(Views.Public.class)
    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    @JsonView(Views.Public.class)
    public List<T> getObjectList() {
        return objectList;
    }

    public void setObjectList(List<T> objectList) {
        this.objectList = objectList;
    }
}
