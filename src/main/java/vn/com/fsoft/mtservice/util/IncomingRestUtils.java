package vn.com.fsoft.mtservice.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;

import vn.com.fsoft.mtservice.constants.CommonConstants;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.form.RestForm;

import java.util.List;

/**
 * 
 * @author hungxoan
 *
 * @param <F>
 */
public class IncomingRestUtils<F extends RestForm> {
	
	public static IncomingRequestContext getContext(RequestEntity requestEntity) {
		
		HttpHeaders requestHeader = requestEntity.getHeaders();
		
		List<String> userIdHeader = null;
		List<String> tokenHeader = null;
		Integer userId = 0;
		String token = CommonConstants.EMPTY;

		if(requestHeader.containsKey("userid")) {
			userIdHeader = requestHeader.get("userid");
			if(userIdHeader != null && userIdHeader.size() > 0) {
				userId = Integer.valueOf(userIdHeader.get(0));
			}
		}

		if(requestHeader.containsKey("token")) {
			tokenHeader = requestHeader.get("token");
			if(tokenHeader != null && tokenHeader.size() > 0) {
				token = tokenHeader.get(0);
			}
		}
		
		IncomingRequestContext context = IncomingRequestContext
				.getInstance(userId, token);

		return context;
	}

	public static HttpHeaders getTokenHeaders(Integer userId, String nonce) {

		HttpHeaders header = new HttpHeaders();
		header.add("UserId", String.valueOf(userId));
		header.add("Nonce", nonce);

		return header;
	}
}	
