package vn.com.fsoft.mtservice.api;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.com.fsoft.mtservice.bean.data.UserRepo;
import vn.com.fsoft.mtservice.bean.manager.LoginHistoryManager;
import vn.com.fsoft.mtservice.bean.sercurity.CredentialTokenService;
import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.controller.ExtraFunctionController;
import vn.com.fsoft.mtservice.object.base.ClientCredential;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.LoginStatus;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.Status;
import vn.com.fsoft.mtservice.object.base.ValidationStatus;
import vn.com.fsoft.mtservice.object.entities.LoginHistoryEntity;
import vn.com.fsoft.mtservice.object.entities.UserEntity;
import vn.com.fsoft.mtservice.object.form.AuthenticationForm;
import vn.com.fsoft.mtservice.object.form.UserForm;
import vn.com.fsoft.mtservice.object.models.UserAuthorityModel;
import vn.com.fsoft.mtservice.services.validator.AuthenticationValidator;
import vn.com.fsoft.mtservice.util.IncomingRestUtils;
import vn.com.fsoft.mtservice.util.JacksonUtils;
import vn.com.fsoft.mtservice.util.NumberUtils;
import vn.com.fsoft.mtservice.util.Views;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author hungxoan
 *
 */

@RestController("authenticationAPI")
@RequestMapping("/api/credential")
public class AuthenticationAPI extends ExtraFunctionController {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private AuthenticationValidator authenticationValidator;

    @Autowired
    private CredentialTokenService credentialTokenService;
    
    @Autowired
    private LoginHistoryManager historyManager;

    @SuppressWarnings("unchecked")
	@PostMapping(value = "/_authenticate",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signIn(RequestEntity<UserForm> requestEntity, HttpServletRequest servlet) {

        // validate
        ValidationStatus validationStatus = authenticationValidator.reject(requestEntity,
                "authentication");
        if(validationStatus != null) {
            return reject(validationStatus);
        }

        UserForm form = requestEntity.getBody();
        AuthenticationForm authenticationForm = form.getAuthentication();

        RepoStatus<UserEntity> userStatus = userRepo
                .findUser(authenticationForm.getUserName(), authenticationForm.getSecret());

        if(userStatus == null || userStatus.getObject() == null) {
            historyManager.addHistory(new LoginHistoryEntity(authenticationForm.getUserName(),
                    Boolean.FALSE, new Date()), servlet);
            return new ResponseEntity<String>(JacksonUtils.java2Json(
                    new ValidationStatus("400",
                            "User name or password is not correct. Please try again")),
                    HttpStatus.BAD_REQUEST);
        }

        UserEntity user = userStatus.getObject();

        // login oke
        // generate token
        historyManager.addHistory(new LoginHistoryEntity(authenticationForm.getUserName(), 
        		Boolean.TRUE, new Date()), servlet);

        ClientCredential credential = credentialTokenService.signIn(user.getId());

        if(credential == null) {

            historyManager.addHistory(new LoginHistoryEntity(authenticationForm.getUserName(),
                    Boolean.FALSE, new Date()), servlet);
            return new ResponseEntity<String>(JacksonUtils.java2Json(
                    new ValidationStatus("400",
                            "Request data is invalid")),
                    HttpStatus.BAD_REQUEST);
        }

        String nonce = credential.getNonce();
        HttpHeaders header = IncomingRestUtils.getTokenHeaders(user.getId(), nonce);

        if(user.getFirstLogin()) {
            return new ResponseEntity<String>(JacksonUtils.java2Json(
                new LoginStatus<>(HttpConstant.CODE_SUCCESS,
                        "found", Boolean.TRUE, user.getProfile().getFullName())
            ), header, HttpStatus.OK);
        }

        // get authorities
        RepoStatus<List<UserAuthorityModel>> authorityStatus = userRepo
                .getUserAuthorities(user.getId());

        if(authorityStatus == null || authorityStatus.getObject() == null) {

            return new ResponseEntity<String>(JacksonUtils.java2Json(
                    new LoginStatus<>("202","not found", user.getFirstLogin(),
                            user.getProfile().getFullName())), header,
                    HttpStatus.OK);
        }

        return new ResponseEntity<String>(JacksonUtils.java2Json(
                new LoginStatus(HttpConstant.CODE_SUCCESS, "found",
                        Boolean.FALSE,  user.getProfile().getFullName(),
                        authorityStatus.getObject()),
                Views.Public.class), header, HttpStatus.OK);
    }

    @GetMapping(value = "/_authorities",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getAuthorities(RequestEntity<UserForm> requestEntity) {

        IncomingRequestContext context = IncomingRestUtils.getContext(requestEntity);
        Integer userId = context.getUserId();
        String token = context.getToken();

        if(NumberUtils.isEmpty(userId)) {
            return new ResponseEntity<String>(JacksonUtils.java2Json(
                    new ValidationStatus("409",
                            "User id is not acceptable")),
                    HttpStatus.BAD_REQUEST);
        }

        if(StringUtils.isEmpty(token) || token.length() < 64) {
            return new ResponseEntity<String>(JacksonUtils.java2Json(
                    new ValidationStatus("409",
                            "Token is invalid")),
                    HttpStatus.BAD_REQUEST);
        }

        // check token
        String cachedToken = credentialTokenService.get(userId);

        if(!cachedToken.equals(token)) {
            return new ResponseEntity<String>(JacksonUtils.java2Json(
                    new ValidationStatus("401",
                            "Request token is expired or not valid anymore. " +
                                    "Please  login again")),
                    HttpStatus.BAD_REQUEST);
        }

        // get authorities
        RepoStatus<List<UserAuthorityModel>> authorityStatus = userRepo.getUserAuthorities(userId);

        if(authorityStatus.getObject() == null) {
            return new ResponseEntity<String>(JacksonUtils.java2Json(
                    new Status("202",
                            "not found")),
                    HttpStatus.BAD_REQUEST);
        }

        ClientCredential credential = credentialTokenService.getClientCredential(userId);
        String nonce = credential.getNonce();
        HttpHeaders header = IncomingRestUtils.getTokenHeaders(userId, nonce);

        return new ResponseEntity<String>(JacksonUtils.java2Json(authorityStatus.getObject(),
                Views.Public.class), header, HttpStatus.OK);
    }
}
